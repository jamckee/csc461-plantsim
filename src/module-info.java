module PlantSim {
        requires javafx.fxml;
        requires javafx.graphics;
        requires javafx.controls;
        requires java.desktop;
        exports mckee_jonathan;
        exports mckee_jonathan.View;
        exports mckee_jonathan.Model;
        }
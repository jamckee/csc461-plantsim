/** Main.java
 *      Plant Simulator
 *
 *  Author: Jonathan McKee
 *  Email: Jonathan.mckee@mines.sdsmt.edu
 *  Date: 25 January 2019
 *
 *  Professor: Dr Lisa Rebenitsch
 *
 *  Course: CSC-468 GUI Programming
 *
 *  Program Description:
 *      Plant simulator is small garden simulation program.  The basis of the program was to have a small graphical
 *  user interface with buttons, labels, a text box, and a combo box surrounding a grid pane that contains an adjustable
 *  sized garden ranging from 3x3 up to 6x6 in a square fashion.
 *
 *  The initial interface would have the following interface options:
 *      New Day - Advances the day counter label, resets the fertilizer buttons, and then performs daily behind the
 *          scene actions on each plant.  These tasks include: Removing daily health, Removing daily moisture, Checking
 *          if a plant is water starved, and removing all dead plants from the scene, and finally updating the remaining
 *          counters for filled boxes and dead plants.
 *      Reset - Resets the game back to day 0.
 *      Update Button and Combo boxes - Re-sizes the garden to a new size without destroying current game state.
 *      Oak, Pine, Cherry - Enables planting a tree mode to plant in available lots.
 *      Daffodil, Tulip, Rose - Enables planting a flower mode to plant flowers in available plots.
 *      Shovel - Enables remove plant mode to remove plants from the selected plots.
 *      Tiller - Tills an empty tile to provide a bonus to next planted plant in that plot.
 *      Water Can - 5 Uses per turn, allows watering a single tile.
 *      Prune - Prunes the plant increasing plant health slightly.
 *
 *      Water All and textbox - Reads the amount of water in the textbox and then applies it across the board to all
 *          plants.
 *      Fertilizer 1 and 2 - Adds 10 health to the next plant clicked on and then disables itself for use until new day.
 *      Super Grow - Single use per turn and has a larger effect then regular fertilizer
 *
 *      Plant Buttons - Each button acts as a hub to display the current tile's information.  In addition, after
 *          enabling the different modes from above, the plant btn then acts as the location selector for the current
 *          mode.
 *
 *      Observer Pattern Implementation:
 *          PropertyChange      PlantBtn.java Line 42
 *          Set Observer        Tile.java   Line 39 (Constructor)
 *                              Tile.java   Line 53 (Update observer)
 *          firePropertyChange  Tile.java   Line 40, 103, 121, 158, 162, 190, 207, 226, 230, 260, 286, 303
 *
 *   Additional added features for points:
 *      10 pts) Additional observer pattern for information panel:
 *          ObsLabel - PropertyChange ObsLabel.java Line 34
 *                     Set Observer   Controller.java Lines 183-185
 *                     firePropertyChange  Controller.java 103, 157, 173, 190, 208, 321, 339, 357, 373, 390, 407, 436
 *                                         GardenModel.java 209, 228, 231, 260, 267, 274, 281, 288, 295, 334
 *
 *      10 pts) Used images instead of text to identify plants
 *
 *
 *   Additional features that may affect testing:
 *      Menu bar - Menu bar added to the top to provide alternate access to all button functions (Except water all)
 *      Cursor Icons - The cursor icon changes to match the plant or tool currently being used or planted
 *      Mnemonic Shortcuts - Buttons have Mnemonic short cuts enabled with the alt key
 *      Plant tiles will display if they have been fertilized, pruned, watered each turn.  Tilled empty tiles change appearance.
 *      Water Can - Only 5 uses per turn
 *      Fertilizer - A plant can only be affected once per turn
 *      Super Grow - Can affect a fertilized plant, but no more then a normal super grow use
 *      Prune - Can only be done once per turn
 *      Changing grid size - Does NOT reset the game field.  Allows the user to increase/decrease size without starting over.
 *              To compensate, a reset button was added under the new day button.
 *      ToolTips - Tool tips after hovering over a button.
 *      Status icons - Plants will display icons after being watered, pruned, or fertilized.
 *
 *   ToDo:
 *      Fix the PlantBtn not resizing properly after creation.
 *
 */

package mckee_jonathan;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * The Main class.
 */
public class Main extends Application {
    //Initital screen size variables
    int initialX = 800;
    int initialY = 600;

    /**
     * Method: Start()
     * Author: Jonathan McKee
     * Starting function that loads the FXML layout file, sets the simulator title, creates a new scene, disables
     * resizing of the main menu, and then displays the the full layout.  In addition we run the code in a try catch
     * block to help get the proper stack traces for debugging if needed.
     *
     * @param primaryStage the primary stage to display
     * @throws Exception An exception is printed to the console if found during playback/testing.
     */
    @Override
    public void start(Stage primaryStage) throws Exception{
        try {
            Parent root = FXMLLoader.load(getClass().getResource("View/layout.fxml"));
            primaryStage.setTitle("Plant Simulator");
            Scene scene = new Scene(root, initialX, initialY);
            primaryStage.setScene(scene);

            //set stage
            primaryStage.show();
        } catch (Exception e){
            //print the stacktrace
            //e.printStackTrace();
            //print the exception
            System.out.println(e);
        }
    }

    /**
     * Method: Main()
     * Author: Jonathan McKee
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}

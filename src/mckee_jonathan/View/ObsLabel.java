/** ObsLabel.java
 * Author: Jonathan McKee
 *
 * Java class file to contain the an inhereted label implementing the observer.
 */
package mckee_jonathan.View;

import javafx.scene.control.Label;
import javafx.scene.text.TextAlignment;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * The custom observer labels used in the simulation layout.
 */
public class ObsLabel extends Label implements PropertyChangeListener{
    /**
     * Constructor: Obs label()
     * Author: Jonathan McKee
     * Instantiates a new Obs label.
     */
    public ObsLabel(){
    }

    /**
     * Method: propertyChange()
     * Author: Jonathan McKee
     * This function is what observers the GardenModel/Tiles for updated information to display.  Each check makes sure
     * only a specific label will update from the current event.
     *
     * @param evt the event that was triggered when firing the property change.
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (this.getId().compareTo("lbl_day") == 0 && evt.getPropertyName().compareTo("newDay") == 0) {
            int temp = (int) evt.getNewValue();
            this.setText(Integer.toString(temp));
        }else if (this.getId().compareTo("lbl_died") == 0 && evt.getPropertyName().compareTo("plantDied") == 0) {
            int temp = (int) evt.getNewValue();
            this.setText(Integer.toString(temp));
        }else if (this.getId().compareTo("lbl_filled") == 0 && evt.getPropertyName().compareTo("plantTotal") == 0) {
            int temp = (int) evt.getNewValue();
            //Should never be negative, but guard against anyway
            if (temp < 0) {temp = 0;} //One line for readability
            this.setText(Integer.toString(temp));
        }else if (this.getId().compareTo("lbl_mode") == 0 && evt.getPropertyName().compareTo("modeChange") == 0){
            String temp = (String) evt.getNewValue();
            //System.out.println(temp);
            this.setText(temp);
            this.setTextAlignment(TextAlignment.CENTER);
        }else if (this.getId().compareTo("lbl_mode") == 1 && evt.getPropertyName().compareTo("reset") == 0){
            this.setText("0");
        }
    }

}

/** PlantBtn.java
 * Author: Jonathan McKee
 *
 * Java class file to contain the Plant button custom class inherited from button but implementing the observer.
 */
package mckee_jonathan.View;

import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.TextAlignment;
import mckee_jonathan.Model.Tile;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import static javafx.scene.control.ContentDisplay.TOP;

/**
 * The Plant button class used in our simulation.
 */
public class PlantBtn extends Button implements PropertyChangeListener {
    /**
     * Constructor: Plant btn()
     * Author: Jonathan McKee
     * Instantiates a new plant btn.
     */
    public PlantBtn(){

    }

    /**
     * Method: PropertyChange()
     * Author: Jonathan McKee
     * Function that checks the subjects for changes and then updates the display information on the plant button to
     * match new data.
     *
     * @param evt the event that triggered the propertyChange.
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        //get the text for the tile
        Tile tempTile = (Tile) evt.getNewValue();
        //Get the tiletype
        String type = (String) evt.getOldValue();

        ImageView tileType = null;
        //set default ground
        ImageView ground;
        Group combinedTile;

        //Check type of tile
        if (type.compareTo("Pine") == 0) {
            tileType = new ImageView(new Image(getClass().getResourceAsStream("/pine.png")));
            ground = new ImageView(new Image(getClass().getResourceAsStream("/planted.png")));
            //System.out.println("The height current height: " + this.getHeight() + ". Current Width: " + this.getWidth());
        }else if (type.compareTo("Oak") == 0) {
            tileType = new ImageView(new Image(getClass().getResourceAsStream("/oak.png")));
            ground = new ImageView(new Image(getClass().getResourceAsStream("/planted.png")));
            //System.out.println("The height current height: " + this.getHeight() + ". Current Width: " + this.getWidth());
        }else if (type.compareTo("Cherry") == 0) {
            tileType = new ImageView(new Image(getClass().getResourceAsStream("/cherry.png")));
            ground = new ImageView(new Image(getClass().getResourceAsStream("/planted.png")));
            //System.out.println("The height current height: " + this.getHeight() + ". Current Width: " + this.getWidth());
        }else if (type.compareTo("daffodil") == 0) {
            tileType = new ImageView(new Image(getClass().getResourceAsStream("/daffodil_f.png")));
            ground = new ImageView(new Image(getClass().getResourceAsStream("/planted.png")));
        }else if (type.compareTo("Rose") == 0){
            tileType = new ImageView(new Image(getClass().getResourceAsStream("/rose_f.png")));
            ground = new ImageView(new Image(getClass().getResourceAsStream("/planted.png")));
        }else if (type.compareTo("Tulip") == 0){
            tileType = new ImageView(new Image(getClass().getResourceAsStream("/tulip_f.png")));
            ground = new ImageView(new Image(getClass().getResourceAsStream("/planted.png")));
        }else if (type.compareTo("Tilled") == 0){
            ground = new ImageView(new Image(getClass().getResourceAsStream("/tilled.png")));
        }else{ //None
            ground = new ImageView(new Image(getClass().getResourceAsStream("/dirt.png")));
        }


        //Calculate offsets from a line of best fit
        //y=1.22x+9.44
        double imgHeight = (this.getHeight()-45);
        double imgWidth;
        if (imgHeight < 35 && evt.getPropertyName().compareTo("created") != 0){
            int temp = Integer.parseInt(evt.getPropertyName());
            switch (temp){
                case 3:
                    imgHeight = 118;
                    break;
                case 4:
                    imgHeight = 78;
                    break;
                case 5:
                    imgHeight = 53;
                    break;
                case 6:
                    imgHeight = 37;
                    break;

            }
        }
        imgWidth = ((1.22*imgHeight) + 9.44);

        //Calculate position for the ground
        ground.setPreserveRatio(true);
        ground.setFitHeight(imgHeight);
        ground.setFitWidth(imgWidth);
        combinedTile = new Group(ground);

        //if we have a tile section, put it in
        if (tileType != null) {
            tileType.setPreserveRatio(true);
            tileType.setFitHeight(imgHeight);
            tileType.setFitWidth(imgWidth);
            combinedTile.getChildren().add(tileType);
        }

        //Check if fertilized
        if (tempTile.isFertilized()){
            ImageView fert = new ImageView(new Image(getClass().getResourceAsStream("/fert.png")));
            ImageView fertStat = new ImageView(new Image(getClass().getResourceAsStream("/status/fertilized.png")));
            fert.setPreserveRatio(true);
            fert.setFitHeight(imgHeight);
            fert.setFitWidth(imgWidth);
            combinedTile.getChildren().add(fert);
            fertStat.setPreserveRatio(true);
            fertStat.setFitHeight(imgHeight);
            fertStat.setFitWidth(imgWidth);
            combinedTile.getChildren().add(fertStat);
        }
        //Check if watered
        if (tempTile.isWatered()){
            ImageView water = new ImageView(new Image(getClass().getResourceAsStream("/water.png")));
            ImageView waterStatus = new ImageView(new Image(getClass().getResourceAsStream("/status/watered.png")));
            water.setPreserveRatio(true);
            water.setFitHeight(imgHeight);
            water.setFitWidth(imgWidth);
            combinedTile.getChildren().add(water);
            waterStatus.setPreserveRatio(true);
            waterStatus.setFitHeight(imgHeight);
            waterStatus.setFitWidth(imgWidth);
            combinedTile.getChildren().add(waterStatus);
        }
        //Check if pruned
        if (tempTile.isPruned()){
            ImageView prune = new ImageView(new Image(getClass().getResourceAsStream("/pruned.png")));
            prune.setPreserveRatio(true);
            prune.setFitHeight(imgHeight);
            prune.setFitWidth(imgWidth);
            combinedTile.getChildren().add(prune);
            ImageView pruneStat = new ImageView(new Image(getClass().getResourceAsStream("/status/pruned.png")));
            pruneStat.setPreserveRatio(true);
            pruneStat.setFitHeight(imgHeight);
            pruneStat.setFitWidth(imgWidth);
            combinedTile.getChildren().add(pruneStat);
        }

        //put it all together on the button
        this.setText(tempTile.toString());
        this.setTextAlignment(TextAlignment.CENTER);
        this.setContentDisplay(TOP);
        this.setGraphic(combinedTile);

    }



}

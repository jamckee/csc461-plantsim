/** GardenView.java
 * Author: Jonathan McKee
 *
 * Java class file for the view functions in the MVC pattern.
 */
package mckee_jonathan.View;

import javafx.event.ActionEvent;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import mckee_jonathan.Controller;
import mckee_jonathan.Model.GardenModel;

/**
 * The Garden view class that extends from GridPane.
 */
public class GardenView extends GridPane {
    /**
     * A variable used to link to the GadenModel.
     */
    private GardenModel garden;

    /**
     * Constructor: Garden view()
     * Author: Jonathan McKee
     *
     * Instantiates a new Garden view by calling the resize function with default values of 3 x 3
     */
    public GardenView(){
        this.resize(3,3);
    }

    /**
     * Method: Resize()
     * Author: Jonathan McKee
     * Resize the garden grid to a new size.
     *
     * @param row    the number of rows
     * @param col    the number of cols
     * @param remote the remote the controller class
     * @return a list containing all of our observers so that we can later link the subjects and observers
     */
    public PlantBtn[][] resize(int row, int col, Controller remote){
        PlantBtn tempBtn;
        String tempId;
        PlantBtn pList[][] = new PlantBtn[col][row];

        //clear the grid pane
        this.getChildren().clear();
        this.getRowConstraints().clear();
        this.getColumnConstraints().clear();
        this.setGridLinesVisible(true);

        //Reset new dimensions
        for (int i = 0; i < col; i++) {
            ColumnConstraints colConst = new ColumnConstraints();
            colConst.setPercentWidth(100.0 / col);
            colConst.setMinWidth(1);
            colConst.setMaxWidth(Double.MAX_VALUE);
            this.getColumnConstraints().add(colConst);
        }
        for (int i = 0; i < row; i++) {
            RowConstraints rowConst = new RowConstraints();
            rowConst.setPercentHeight(100.0 / row);
            rowConst.setMinHeight(1);
            rowConst.setMaxHeight(Double.MAX_VALUE);
            this.getRowConstraints().add(rowConst);
        }
        //Add buttons for garden
        for (int i = 0; i < row; i++){
            for (int j = 0; j < col; j++){
                tempId = "pb" + i + j;
                tempBtn = new PlantBtn();
                tempBtn.setVisible(true);
                //All buttons start as empty
                tempBtn.setText("None");
                tempBtn.setMinWidth(1);
                tempBtn.setMinHeight(1);
                tempBtn.setMaxWidth(Double.MAX_VALUE);
                tempBtn.setMaxHeight(Double.MAX_VALUE);
                tempBtn.setId(tempId);
                tempBtn.addEventHandler(ActionEvent.ANY, remote.new plantHandler() );
                /*tempBtn.setOnAction(e -> {
                    Node source = (Node) e.getSource();
                    System.out.println("My id is: " + source.getId());
                });*/
                this.add(tempBtn,i,j);
                pList[i][j] = tempBtn;
            }
        }

        return pList;
    }

    /**
     * Method: setGarden()
     * Author: Jonathan McKee
     * Sets our garden variable to point to the GardenModel being used for the simulation.
     *
     * @param newGarden the new garden
     */
    public void setGarden( GardenModel newGarden){
        garden = newGarden;
    }
}

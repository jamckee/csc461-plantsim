/** Controller.java
 * Author: Jonathan McKee
 *
 * Java class file that contains the code for our controller.
 */
package mckee_jonathan;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
//Standard control items
import javafx.scene.Cursor;
import javafx.scene.ImageCursor;
import javafx.scene.Scene;
import javafx.scene.control.*;
//Custom control items
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import mckee_jonathan.Model.GardenModel;
import mckee_jonathan.View.GardenView;
import mckee_jonathan.View.ObsLabel;
import mckee_jonathan.View.PlantBtn;
//Observer help
import java.beans.PropertyChangeSupport;


/**
 * The Controller class.
 */
public class Controller {
    /**
     * Public controller accessors
     */
    public Button btn_fert1; // The 1st fertilizer bag.
    public Button btn_fert2; // The 2nd fertilizer bag
    public Button btn_fert3; // The 3rd fertilizer bag/super grow
    public Button btn_newDay; // Move to a new day
    public Button btn_update; // Resizes the current garden
    public Button btn_reset; //reset button
    public Button btn_water; // Water's all plants
    public Button btn_watercan; // Water a specific plant
    public ChoiceBox<String> cb_size; // The size of the garden during update
    public GardenView gp_garden; // The actual garden grid
    public ObsLabel lbl_day; // Observer label for current day
    public ObsLabel lbl_died; // Observer label for total number of dead plants
    public ObsLabel lbl_filled; // Observer label for total number of current plants
    public ObsLabel lbl_mode;  //display label of the current mode
    public Label lbl_mode_pic; //Image for mode
    public TextField tb_water; //Textfield box to enter how much water all does

    public Button btn_pine; //Plant Pine tree
    public Button btn_oak; //Plant Oak tre
    public Button btn_cherry; //Plant Cherry Tree
    public Button btn_daffodil; //Plant daffodil
    public Button btn_rose; //Plant Rose
    public Button btn_tulip; //Plant Tulip

    public Button btn_shovel; //Use shovel
    public Button btn_prune;  //Use pruners
    public Button btn_tiller; //use tiller
    public Button btn_aligner; //Used to align supergrow button

    /**
     * Public image resources
     */
    private Image iCherry = new Image(getClass().getResourceAsStream("/icons/cherry.png"));  //pass in the image path
    private Image iDaffodil = new Image(getClass().getResourceAsStream("/icons/daffodil.png"));  //pass in the image path
    private Image iRose = new Image(getClass().getResourceAsStream("/icons/rose.png"));  //pass in the image path
    private Image iOak = new Image(getClass().getResourceAsStream("/icons/oak.png"));  //pass in the image path
    private Image iPine = new Image(getClass().getResourceAsStream("/icons/pine.png"));  //pass in the image path
    private Image iTulip = new Image(getClass().getResourceAsStream("/icons/tulip.png"));  //pass in the image path

    private Image iWCan = new Image(getClass().getResourceAsStream("/icons/wcan.png"));  //pass in the image path
    private Image iShovel = new Image(getClass().getResourceAsStream("/icons/shovel.png"));  //pass in the image path
    private Image iTiller = new Image(getClass().getResourceAsStream("/icons/tiller.png"));  //pass in the image path
    private Image iPruner = new Image(getClass().getResourceAsStream("/icons/prune.png"));  //pass in the image path

    private Image iFert1 = new Image(getClass().getResourceAsStream("/icons/fert1.png"));  //pass in the image path
    private Image iFert2 = new Image(getClass().getResourceAsStream("/icons/fert2.png"));  //pass in the image path
    private Image iSGrow = new Image(getClass().getResourceAsStream("/icons/grow.png"));  //pass in the image path


    private Scene scn_main;  //Variable to store scene
    private GardenModel gardenBackend; //The data model used for the program
    private String mode = "watch"; //The current tool mode, watch is default behavior
    private PropertyChangeSupport subject;

    /**
     * Method: Initialize()
     * Author: Jonathan McKee
     * Initialize, it get's a new garden, and then sets the view observer to watch that value more closely.  In addition
     * we save our pList for later use.
     */
    //run our startup scripts
    @FXML
    public void initialize() {
        //Setup our observer to watch the controller
        subject = new PropertyChangeSupport(this);
        subject.addPropertyChangeListener(lbl_mode);
        subject.addPropertyChangeListener(lbl_day);
        subject.addPropertyChangeListener(lbl_died);
        subject.addPropertyChangeListener(lbl_filled);

        //link the garden view and model
        gp_garden.setGarden(gardenBackend);

        //resize the garden to default
        PlantBtn[][] pList;
        pList = gp_garden.resize(3, 3, this);

        //Create our Model and View objects
        gardenBackend = new GardenModel(pList);
        resetGame();

        //setup button graphics
        buttonIcons(btn_watercan, iWCan);
        buttonTooltips(btn_watercan, "Watering can for a single plant.");
        buttonIcons(btn_tiller, iTiller);
        buttonTooltips(btn_tiller, "Tiller to prepare empty plot for a new plant.");
        buttonIcons(btn_prune, iPruner);
        buttonTooltips(btn_prune, "Pruner to style plant and give a small health boost.");
        buttonIcons(btn_shovel, iShovel);
        buttonTooltips(btn_shovel, "Shovel to remove the selected plant.");

        buttonIcons(btn_daffodil, iDaffodil);
        buttonTooltips(btn_daffodil, "Plant a daffodil.");
        buttonIcons(btn_rose, iRose);
        buttonTooltips(btn_rose, "Plant a rose.");
        buttonIcons(btn_tulip, iTulip);
        buttonTooltips(btn_tulip, "Plant a tulip.");

        buttonIcons(btn_oak, iOak);
        buttonTooltips(btn_oak, "Plant an Oak tree.");
        buttonIcons(btn_pine, iPine);
        buttonTooltips(btn_pine, "Plant a Pine tree.");
        buttonIcons(btn_cherry, iCherry);
        buttonTooltips(btn_cherry, "Plant a Cherry tree.");

        buttonIcons(btn_fert1, iFert1);
        buttonTooltips(btn_fert1, "Fertilizer Bag#1. Gives plant a small health boost.");
        buttonIcons(btn_fert2, iFert2);
        buttonTooltips(btn_fert2, "Fertilizer Bag#2. Gives plant a small health boost.");
        btn_aligner.setMinSize(btn_fert3.getWidth(), btn_fert3.getHeight());
        btn_aligner.setMaxSize(btn_fert3.getWidth(), btn_fert3.getHeight());
        buttonIcons(btn_fert3, iSGrow);
        buttonTooltips(btn_fert3, "Super Grow. Gives plant a large health boost.");

        buttonTooltips(btn_newDay, "Begins a new day.");
        buttonTooltips(btn_update, "Change the grid size.");
        buttonTooltips(btn_reset, "Reset the game back to day 0.");


    }


    /**
     * Constructor: Controller()
     * Author: Jonathan McKee
     * Instantiates a new Controller.
     */
    public Controller() {
    }

    /**
     * Method: buttonIcons()
     * Author: Jonathan McKee
     *
     * Set's the icon graphics for seed and tool buttons
     */
    private void buttonIcons(Button btn, Image img){
        double size = btn.getWidth();
        if (size == 0.0)
            size = 45.0;
        ImageView temp = new ImageView(img);
        temp.setFitWidth(size);
        temp.setFitHeight(size);
        btn.setGraphic(temp);
        btn.setText("");
    }
    /**
     * Method: buttonTooltips()
     * Author: Jonathan McKee
     *
     * Set's the button tooltips for seed and tool buttons
     */
    private void buttonTooltips(Button btn, String str){
        btn.setTooltip(new Tooltip(str));
    }

    /**
     * Method: clearMode()
     * Author: Jonathan McKee
     * Resets mode to watch and clears the watch label text.
     */
    private void clearMode(){
        //lbl_mode.setText("");
        subject.firePropertyChange("modeChange", "anything", "");
        lbl_mode_pic.setVisible(false);
        mode = "watch";
        if (scn_main != null){
            scn_main.setCursor(Cursor.DEFAULT);
        }else{
            //Try and set scene
            scn_main = btn_newDay.getScene();
            //Try and clear cursor again
            if (scn_main != null)
                scn_main.setCursor(Cursor.DEFAULT);
        }

    }

    /**
     * Method: closeAction()
     * Author: Jonathan McKee
     * This function closes the main window
     */
    public void closeAction(){
        // bull the current stage from newDay button
        Stage stage = (Stage) btn_newDay.getScene().getWindow();
        // do what you have to do
        stage.close();
    }

    /**
     * Method: Fertilize()
     * Author: Jonathan McKee
     *
     * Switches the mode to fertilizer mode based on the source button
     *
     * @param id the of the fert bag selected.
     */
    public void fertilize(String id) {
        if (!mode.equals(id)) {
            mode = id;
            Image image;
            Image image2;
            String newVal = "Fertilizer";
            if (id.compareTo("fert1") == 0){
                newVal = "Using:\nFertilizer Bag#1";
                image = new Image(getClass().getResourceAsStream("/icons/fert1.png"));  //pass in the image path
                image2 = new Image(getClass().getResourceAsStream("/icons/fert1.png"));
            }else if (id.compareTo("fert2") == 0){
                newVal = "Using:\nFertilizer Bag#2";
                image = new Image(getClass().getResourceAsStream("/icons/fert2.png"));  //pass in the image path
                image2 = new Image(getClass().getResourceAsStream("/icons/fert2.png"));
            }else if (id.compareTo("fert3") == 0){
                newVal = "Using:\nSuper Grow";
                image = new Image(getClass().getResourceAsStream("/icons/grow.png"));  //pass in the image path
                image2 = new Image(getClass().getResourceAsStream("/icons/grow.png"));
            }else{
                image = new Image(getClass().getResourceAsStream("/icons/fert.png"));  //pass in the image path
                image2 = new Image(getClass().getResourceAsStream("/icons/fert.png"));
            }
            subject.firePropertyChange("modeChange", "anything", newVal);
            lbl_mode_pic.setVisible(true);
            ImageView img = new ImageView(image2);
            img.setFitHeight(lbl_mode_pic.getWidth());
            img.setFitWidth(lbl_mode_pic.getWidth());
            lbl_mode_pic.setGraphic(img);
            btn_newDay.getScene().setCursor(new ImageCursor(image));
        } else {
            clearMode();
        }
    }

    /**
     * Method: Fertilize()
     * Author: Jonathan McKee
     *
     * Gets the fertilizer mode from the menu item id number
     * @param e the event that was passed to this action.
     */
    public void fertilizeMenu(ActionEvent e) {
        MenuItem menu = (MenuItem) e.getSource();
        String id = menu.getId();
        id = id.replaceFirst("men","");
        fertilize(id);
    }
    /**
     * Method: Fertilize()
     * Author: Jonathan McKee
     *
     *  Gets the fertilizer mode from the buttons
     *
     * @param e the event that was passed to this action.
     */
    public void fertilizeBtn(ActionEvent e) {
        Button btn = (Button) e.getSource();
        String id = btn.getId().replaceFirst("^btn_", "");
        fertilize(id);
    }

    /**
     * Method: plantDaffodil()
     * Author: Jonathan McKee
     * Sets our mode to flower planting.
     */
    public void plantDaffodil() {
        if (!mode.equals("daffodil")) {
            mode = "daffodil";
            subject.firePropertyChange("modeChange", "anything", "Planting: \nDaffodils");
            Image image = new Image(getClass().getResourceAsStream("/icons/daffodil.png"));  //pass in the image path
            Image image2 = new Image(getClass().getResourceAsStream("/icons/daffodil.png"));  //pass in the image path

            btn_newDay.getScene().setCursor(new ImageCursor(image));
            lbl_mode_pic.setVisible(true);
            ImageView img = new ImageView(image2);
            img.setFitHeight(lbl_mode_pic.getWidth());
            img.setFitWidth(lbl_mode_pic.getWidth());
            lbl_mode_pic.setGraphic(img);
        } else {
            clearMode();
        }
    }
    /**
     * Method: plantRose()
     * Author: Jonathan McKee
     * Sets our mode to flower planting.
     */
    public void plantRose() {
        if (!mode.equals("Rose")){
            mode = "Rose";
            subject.firePropertyChange("modeChange", "anything", "Planting: \nRoses");
            Image image = new Image(getClass().getResourceAsStream("/icons/rose.png"));  //pass in the image path
            Image image2 = new Image(getClass().getResourceAsStream("/icons/rose.png"));  //pass in the image path

            btn_newDay.getScene().setCursor(new ImageCursor(image));
            //lbl_mode.setText("*** daffodil Planting Activated ***");
            //System.out.println("daffodil mode activated.");
            lbl_mode_pic.setVisible(true);
            ImageView img = new ImageView(image2);
            img.setFitHeight(lbl_mode_pic.getWidth());
            img.setFitWidth(lbl_mode_pic.getWidth());
            lbl_mode_pic.setGraphic(img);
        } else {
            clearMode();
        }
    }

    /**
     * Method: plantTulip()
     * Author: Jonathan McKee
     * Sets our mode to flower planting.
     */
    public void plantTulip() {
        if (!mode.equals("Tulip")){
            mode = "Tulip";
            subject.firePropertyChange("modeChange", "anything", "Planting:\nTulips");
            Image image = new Image(getClass().getResourceAsStream("/icons/tulip.png"));  //pass in the image path
            Image image2 = new Image(getClass().getResourceAsStream("/icons/tulip.png"));  //pass in the image path
            btn_newDay.getScene().setCursor(new ImageCursor(image));
            //lbl_mode.setText("*** daffodil Planting Activated ***");
            //System.out.println("daffodil mode activated.");
            lbl_mode_pic.setVisible(true);
            ImageView img = new ImageView(image2);
            img.setFitHeight(lbl_mode_pic.getWidth());
            img.setFitWidth(lbl_mode_pic.getWidth());
            lbl_mode_pic.setGraphic(img);
        } else {
            clearMode();
        }
    }


    /**
     * Method: New day()
     * Author: Jonathan McKee
     * Checks and runs the newDay script from the attached gardenModel.  After the individual components finish running
     * their individual newDay scripts, this script then resets our buttons back to available and resets our set water
     * combo box t
     */
    public void newDay() {
        //Call the backend's newDay function and directly push the returned day value into the label
        //lbl_day.setText(Integer.toString(gardenBackend.newDay()));
        gardenBackend.newDay();
        //clear any mode we are in
        clearMode();
        //Re-enable our fertilizer buttons
        btn_fert1.setDisable(false);
        btn_fert2.setDisable(false);
        btn_fert3.setDisable(false);
        btn_watercan.setDisable(false);
        //default water to 0
        tb_water.setText("0");
    }

    /**
     * Method: resetGame()
     * Author: Jonathan McKee
     *
     * This function resets all game functionality back to their defaultss.
     */
    public void resetGame(){
        //set mode back to watch
        clearMode();
        PlantBtn[][] pList;
        //pull size from list
        int size = Integer.parseInt(cb_size.getValue().toString().substring(0, 1));
        //draw the new garden
        pList = gp_garden.resize(size, size, this);
        gardenBackend = new GardenModel(size, size, pList);
        //give the frontend the new garden
        gp_garden.setGarden(gardenBackend);
        //reset backend data watchers
        gardenBackend.setObserver(lbl_day);
        gardenBackend.setObserver(lbl_died);
        gardenBackend.setObserver(lbl_filled);
        tb_water.setText("0");
        btn_fert1.setDisable(false);
        btn_fert2.setDisable(false);
        btn_fert3.setDisable(false);
        btn_watercan.setDisable(false);
        //reset display values
        lbl_day.setText("0");
        lbl_died.setText("0");
        lbl_filled.setText("0");
    }

    /**
     * Method: Resize()
     * Author: Jonathan McKee
     * Alows us to resize the current grid to a new size.
     */
    public void resize() {
        //Future improvement: resize garden and not delete it
        GardenModel temp = gardenBackend;
        int size = Integer.parseInt(cb_size.getValue().toString().substring(0,1));
        PlantBtn[][] pList;

        pList = gp_garden.resize(size, size, this);
        gardenBackend = new GardenModel(size, size, pList, temp);

        //reset the garden model/view links
        gp_garden.setGarden(gardenBackend);
        //reset backend data watchers
        gardenBackend.setObserver(lbl_day);
        gardenBackend.setObserver(lbl_died);
        gardenBackend.setObserver(lbl_filled);

        gardenBackend.triggerUpdate();
    }
    /**
     * Method: setSize()
     * Author: Jonathan McKee
     * Alows us to resize the current grid to a new size.
     */
    public void setSize(ActionEvent e) {
        MenuItem temp = (MenuItem) e.getSource();
        String val = temp.getId();
        //Future improvement: resize garden and not delete it
        if (val.compareTo("menThree") == 0){
            cb_size.setValue("3x3 (9)");
        }else if (val.compareTo("menFour") == 0){
            cb_size.setValue("4x4 (16)");
        }else if (val.compareTo("menFive") == 0){
            cb_size.setValue("5x5 (25)");
        }else if (val.compareTo("menSix") == 0){
            cb_size.setValue("6x6 (36)");
        }
    }
    /**
     * Method: Till()
     * Author: Jonathan McKee
     * This function attempt to remove a plant/tile from the garden grid.
     */
    public void till() {
        if (!mode.equals("till")){
            mode = "till";
            subject.firePropertyChange("modeChange", "anything", "Using:\nTiller");
            Image image = new Image(getClass().getResourceAsStream("/icons/tiller.png"));  //pass in the image path
            Image image2 = new Image(getClass().getResourceAsStream("/icons/tiller.png"));  //pass in the image path
            btn_newDay.getScene().setCursor(new ImageCursor(image));
            //lbl_mode.setText("*** Tiller Activated ***");
            //System.out.println("Till mode activated.");
            lbl_mode_pic.setVisible(true);
            ImageView img = new ImageView(image2);
            img.setFitHeight(lbl_mode_pic.getWidth());
            img.setFitWidth(lbl_mode_pic.getWidth());
            lbl_mode_pic.setGraphic(img);
        } else {
            clearMode();
        }
    }

    /**
     * Method: Shovel()
     * Author: Jonathan McKee
     * This function attempt to remove a plant/tile from the garden grid.
     */
    public void shovel() {
        if (!mode.equals("shovel")){
            mode = "shovel";
            subject.firePropertyChange("modeChange", "anything", "Using:\nShovel");
            Image image = new Image(getClass().getResourceAsStream("/icons/shovel.png"));  //pass in the image path
            Image image2 = new Image(getClass().getResourceAsStream("/icons/shovel.png"));  //pass in the image path
            btn_newDay.getScene().setCursor(new ImageCursor(image));
            //lbl_mode.setText("*** Tiller Activated ***");
            //System.out.println("Till mode activated.");
            lbl_mode_pic.setVisible(true);
            ImageView img = new ImageView(image2);
            img.setFitHeight(lbl_mode_pic.getWidth());
            img.setFitWidth(lbl_mode_pic.getWidth());
            lbl_mode_pic.setGraphic(img);
        } else {
            clearMode();
        }
    }

    /**
     * Method: Prune()
     * Author: Jonathan McKee
     * This function attempts to prune a plant.
     */
    public void prune() {
        if (!mode.equals("prune")){
            mode = "prune";
            subject.firePropertyChange("modeChange", "anything", "Using:\nPruners");
            Image image = new Image(getClass().getResourceAsStream("/icons/prune.png"));  //pass in the image path
            Image image2 = new Image(getClass().getResourceAsStream("/icons/prune.png"));  //pass in the image path
            btn_newDay.getScene().setCursor(new ImageCursor(image));
            lbl_mode_pic.setVisible(true);
            ImageView img = new ImageView(image2);
            img.setFitHeight(lbl_mode_pic.getWidth());
            img.setFitWidth(lbl_mode_pic.getWidth());
            lbl_mode_pic.setGraphic(img);
        } else {
            clearMode();
        }
    }

    /**
     * Method: plantPine()
     * Author: Jonathan McKee
     * This function sets or clears the tree mode.
     */
    public void plantPine() {
        if (!mode.equals("pine")){
            mode = "pine";
            subject.firePropertyChange("modeChange", "anything", "Planting:\nPine Tree");
            Image image = new Image(getClass().getResourceAsStream("/icons/pine.png"));  //pass in the image path
            Image image2 = new Image(getClass().getResourceAsStream("/icons/pine.png"));  //pass in the image path
            btn_newDay.getScene().setCursor(new ImageCursor(image));
            //lbl_mode.setText("*** Tree Planting Activated ***");
            //System.out.println("Tree mode activated.");
            lbl_mode_pic.setVisible(true);
            ImageView img = new ImageView(image2);
            img.setFitHeight(lbl_mode_pic.getWidth());
            img.setFitWidth(lbl_mode_pic.getWidth());
            lbl_mode_pic.setGraphic(img);
        } else {
            clearMode();
        }
    }
    /**
     * Method: plantOak()
     * Author: Jonathan McKee
     * This function sets or clears the tree mode.
     */
    public void plantOak() {
        if (!mode.equals("oak")) {
            mode = "oak";
            subject.firePropertyChange("modeChange", "anything", "Planting:\nOak Tree");
            Image image = new Image(getClass().getResourceAsStream("/icons/oak.png"));  //pass in the image path
            Image image2 = new Image(getClass().getResourceAsStream("/icons/oak.png"));  //pass in the image path
            btn_newDay.getScene().setCursor(new ImageCursor(image));
            //lbl_mode.setText("*** Tree Planting Activated ***");
            //System.out.println("Tree mode activated.");
            lbl_mode_pic.setVisible(true);
            ImageView img = new ImageView(image2);
            img.setFitHeight(lbl_mode_pic.getWidth());
            img.setFitWidth(lbl_mode_pic.getWidth());
            lbl_mode_pic.setGraphic(img);
        } else {
            clearMode();
        }
    }
    /**
     * Method: plantCherry()
     * Author: Jonathan McKee
     * This function sets or clears the tree mode.
     */
    public void plantCherry() {
        if (!mode.equals("cherry")){
            mode = "cherry";
            subject.firePropertyChange("modeChange", "anything", "Planting:\nCherry Tree");
            Image image = new Image(getClass().getResourceAsStream("/icons/cherry.png"));  //pass in the image path
            Image image2 = new Image(getClass().getResourceAsStream("/icons/cherry.png"));  //pass in the image path
            btn_newDay.getScene().setCursor(new ImageCursor(image));
            //lbl_mode.setText("*** Tree Planting Activated ***");
            //System.out.println("Tree mode activated.");
            lbl_mode_pic.setVisible(true);
            ImageView img = new ImageView(image2);
            img.setFitHeight(lbl_mode_pic.getWidth());
            img.setFitWidth(lbl_mode_pic.getWidth());
            lbl_mode_pic.setGraphic(img);
        } else {
            clearMode();
        }

    }
    /**
     * Method: Water()
     * Author: Jonathan McKee
     * Attempts to water all tiles inside of the garden.
     */
    public void water() {
        clearMode();
        double water = Double.parseDouble(tb_water.getText());
        gardenBackend.water(water);
        tb_water.setText("0");
    }

    /**
     * Method: Watercan()
     * Author: Jonathan McKee
     * This function sets or cleqrs the current mode.
     */
    public void watercan() {
        if (!mode.equals("watercan")){
            mode = "watercan";
            subject.firePropertyChange("modeChange", "anything", "Using:\nWatering Can");
            //lbl_mode.setText("*** Watering Can Activated ***");
            //System.out.println("Tree mode activated.");
            Image image = new Image(getClass().getResourceAsStream("/icons/wcan.png"));  //pass in the image path
            Image image2 = new Image(getClass().getResourceAsStream("/icons/wcan.png"));  //pass in the image path
            btn_newDay.getScene().setCursor(new ImageCursor(image));
            lbl_mode_pic.setVisible(true);
            ImageView img = new ImageView(image2);
            img.setFitHeight(lbl_mode_pic.getWidth());
            img.setFitWidth(lbl_mode_pic.getWidth());
            lbl_mode_pic.setGraphic(img);
        } else {
            clearMode();
        }
    }

    /**
     * The Plant handler class is a nestged class that implements the event handler.
     */
    public class plantHandler implements EventHandler<ActionEvent> {
        /**
         * Method: Handle()
         * Author: Jonathan McKee
         *
         * Constructor for our actional handler used by the plant btns.  It first checks to see if the function
         *
         * that called this a plantbtn, and then checks to see what mode we are in to determine how this
         * event handler will process the clicks to the garden buttons.
         *
         * @param e the e is the event that triggered the handler to run.
         */
        @Override
        public void handle(ActionEvent e) {
            PlantBtn btn = (PlantBtn) e.getSource();
            String id = btn.getId();

            //strip out the btn coords
            int x = Character.getNumericValue(id.charAt(3));
            int y = Character.getNumericValue(id.charAt(2));

            //System.out.println("Button click reported from: " + id + " at: (" + x + "," + y + ") with mode: " + mode);
            if (mode.compareTo("daffodil") == 0) {
                gardenBackend.plantSeed(x, y, 1);
            } else if (mode.compareTo("Rose") == 0) {
                gardenBackend.plantSeed(x, y, 2);
            } else if (mode.compareTo("Tulip") == 0) {
                gardenBackend.plantSeed(x, y, 3);
            } else if (mode.compareTo("pine") == 0) {
                gardenBackend.plantSeed(x, y, 11);
            } else if (mode.compareTo("oak") == 0) {
                gardenBackend.plantSeed(x, y, 12);
            } else if (mode.compareTo("cherry") == 0) {
                gardenBackend.plantSeed(x, y, 13);
            } else if (mode.compareTo("till") == 0) {
                gardenBackend.useTool(x, y, 1);
            } else if (mode.compareTo("shovel") == 0) {
                gardenBackend.useTool(x, y, 0);
            } else if (mode.compareTo("prune") == 0) {
                gardenBackend.useTool(x, y, 3);
            } else if (mode.compareTo("fert1") == 0) {
                if (gardenBackend.fertilize(x, y, 0)) {
                    btn_fert1.setDisable(true);
                }
                clearMode();
            } else if (mode.compareTo("fert2") == 0) {
                if (gardenBackend.fertilize(x, y, 1)) {
                    btn_fert2.setDisable(true);
                }
                clearMode();
            } else if (mode.compareTo("fert3") == 0) {
                if (gardenBackend.fertilize(x, y, 2)){
                    btn_fert3.setDisable(true);
                }
                clearMode();
            } else if (mode.compareTo("watercan") == 0) {
                if (gardenBackend.water(x, y, 5) == 0){
                    clearMode();
                    btn_watercan.setDisable(true);
                }
            }
        }

    }
}

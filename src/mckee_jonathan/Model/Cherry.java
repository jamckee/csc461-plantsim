/** Empty.java
 * Author: Jonathan McKee
 *
 * Java class file to contain the Tree plot.
 */
package mckee_jonathan.Model;

/**
 * The Cherry class extended from Tree.
 */
public class Cherry extends Tree{
    /**
     * Constructor: Cherry()
     * Author: Jonathan McKee
     *
     * Instantiates a new Tree and sets its default values.
     */
    public Cherry(){
        this.setHealth(30);
        this.setWater(10);
        this.setName("Cherry");
        this.setWatered(false);
        this.setPruned(false);
        this.setTilled(false);
    }

    /**
     * Method: toString()
     * Author: Jonathan McKee
     * Overrides the default toString method to return the information
     * string of the class information.
     *
     * @return the new string
     */
    public String toString(){
        String temp = "Health:" + getHealth();
        return temp;
    }

    /**
     * Method: fertilize()
     * Author: Jonathan McKee
     * This method overrides the default fertilize behavior, because fertilizer is super effective for the
     * cherry tree.
     *
     * @return the boolean value
     */
    @Override
    public boolean fertilize(double amount, boolean supergrow) {
        if (!supergrow)
            amount *= 1.5;
        if (getHealth() > 0 && !this.isFertilized()) {
            addHealth(amount);
            this.setFertilized(true);
            return true;
        } else if(getHealth() > 0 && this.isFertilized() && supergrow){
            //supergrow only 50% effective if already fertilized
            addHealth(amount/2);
            setFertilized(true);
            return true;
        }
        return false;
    }
}

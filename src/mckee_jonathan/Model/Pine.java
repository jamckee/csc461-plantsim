/** Empty.java
 * Author: Jonathan McKee
 *
 * Java class file to contain the Tree plot.
 */
package mckee_jonathan.Model;

/**
 * The Pine class extended from Tree.
 */
public class Pine extends Tree{
    /**
     * Constructor: Pine()
     * Author: Jonathan McKee
     *
     * Instantiates a new Tree and sets its default values.
     */
    public Pine(){
        this.setHealth(40);
        this.setWater(15);
        this.setName("Pine");
        this.setWatered(false);
        this.setPruned(false);
        this.setTilled(false);
    }

    /**
     * Method: toString()
     * Author: Jonathan McKee
     * Overrides the default toString method to return the information
     * string of the class information.
     *
     * @return the new string
     */
    public String toString(){
        String temp = "Health:" + getHealth();
        return temp;
    }
}

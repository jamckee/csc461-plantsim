/** Empty.java
 * Author: Jonathan McKee
 *
 * Java class file to contain the Tree plot.
 */
package mckee_jonathan.Model;

/**
 * The Oak class extended from Tree.
 */
public class Oak extends Tree{
    /**
     * Constructor: Oak()
     * Author: Jonathan McKee
     *
     * Instantiates a new Tree and sets its default values.
     */
    public Oak(){
        this.setHealth(60);
        this.setWater(30);
        this.setName("Oak");
        this.setWatered(false);
        this.setPruned(false);
        this.setTilled(false);
    }

    /**
     * Method: toString()
     * Author: Jonathan McKee
     * Overrides the default toString method to return the information
     * string of the class information.
     *
     * @return the new string
     */
    public String toString(){
        String temp = "Health:" + getHealth();
        return temp;
    }
}

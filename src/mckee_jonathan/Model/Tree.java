/** Empty.java
 * Author: Jonathan McKee
 *
 * Java class file to contain the Tree plot.
 */
package mckee_jonathan.Model;

/**
 * The Tree class extended from Plant.
 */
public class Tree extends Plant{
    /**
     * Constructor: Tree()
     * Author: Jonathan McKee
     *
     * Instantiates a new Tree and sets its default values.
     */
    public Tree(){
        this.setHealth(50);
        this.setWater(20);
        this.setName("Tree");
        this.setWatered(false);
        this.setPruned(false);
        this.setTilled(false);
    }

    /**
     * Method: toString()
     * Author: Jonathan McKee
     * Overrides the default toString method to return the information
     * string of the class information.
     *
     * @return the new string
     */
    public String toString(){
        String temp = "Health:" + getHealth();
        return temp;
    }
}

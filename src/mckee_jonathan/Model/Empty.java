/** Empty.java
 * Author: Jonathan McKee
 *
 * java class file to contain the empty plot.
 */
package mckee_jonathan.Model;


/**
 * The Empty class extending from Plant.
 */
public class Empty extends Plant{
    /**
     * Constructor: Empty()
     * Author: Jonathan McKee
     *
     * Instantiates a new Empty plant and sets the name equal to None.
     */
    public Empty(){
        setName("None");
    }

    /**
     * Method: toString()
     * Author: Jonathan McKee
     *
     * Overrides the default toString operator for the Empty Class.
     *
     * @return the name of this plant
     */
    public String toString(){
        return "None";
    }
}

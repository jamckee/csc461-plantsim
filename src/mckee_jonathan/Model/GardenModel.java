/** GardenModel.java
 * Author: Jonathan McKee
 *
 * java class file to contain the GardenModel for our Garden Data.
 */
package mckee_jonathan.Model;

import mckee_jonathan.View.ObsLabel;
import mckee_jonathan.View.PlantBtn;

import java.beans.PropertyChangeSupport;

/**
 * The GardenModel class that acts as the data manager for the plant simulation.
 */
public class GardenModel {
    /**
     * The alive plant counter.
     */
    private int aliveCnt;
    /**
     * The available fertilizer counter.
     */
    private double[] availFert = new double[3];
    /**
     * The current day counter.
     */
    private int dayCnt;
    /**
     * The dead plant counter.
     */
    private int deadCnt;
    /**
     * The array of garden plots.
     */
    private Tile[][] plants;
    /**
     * The member variable for the observer pattern.
     */
    private PropertyChangeSupport subject;
    /**
     * The amount of water the water can has left.
     */
    private int waterCanLevel;
    /**
     * The size of the garden
     */
    private int gardenSize;

    /**
     * Constructor: Garden model()
     * Author: Jonathan McKee
     *
     * Calls the other gardenModel constructor feeding in default garden size of 3x3
     *
     * @param pList the plant button list used to link our observer and subjects
     */
    public GardenModel(PlantBtn[][] pList){
        this(3,3, pList);
    }

    /**
     * Constructor: Garden model()
     * Author: Jonathan McKee
     *
     * Instantiates a new Garden Model and initializes all of our variables.  In addition,
     * this function links all of our newly created tile's with the appropriate observer
     * from the PlantBtn in the view.
     *
     * @param x     the x size value
     * @param y     the y size value
     * @param pList the plant button list used to link our observer and subjects
     */
    public GardenModel(int x, int y, PlantBtn[][] pList){
        dayCnt = 0;
        deadCnt = 0;
        aliveCnt = 0;
        plants = new Tile[y][x];
        availFert[0] = 1;
        availFert[1] = 1;
        availFert[2] = 1;
        waterCanLevel = 5;
        gardenSize = x;
        subject = new PropertyChangeSupport(this);

        for (int i = 0; i < y; i++){
            for (int j = 0; j < x; j++){
                plants[i][j] = new Tile(pList[i][j]);
            }
        }
    }
    /**
     * Constructor: Garden model()
     * Author: Jonathan McKee
     *
     * Gardenmodel constructor that takes in an oldGarden as a parameter to copy constructor.
     *
     * @param x     the x size value
     * @param y     the y size value
     * @param pList the plant button list used to link our observer and subjects
     */
    public GardenModel(int x, int y, PlantBtn[][] pList, GardenModel oldGardenModel){
        dayCnt = oldGardenModel.getDay();
        deadCnt = oldGardenModel.getDead();
        aliveCnt = oldGardenModel.getAlive();
        Tile[][] oldGarden = oldGardenModel.getGarden();
        plants = new Tile[y][x];
        availFert[0] = 1;
        availFert[1] = 1;
        availFert[2] = 1;
        waterCanLevel = 5;
        gardenSize = x;
        subject = new PropertyChangeSupport(this);
        int oldSize = oldGarden[0].length;

        for (int i = 0; i < y; i++){
            for (int j = 0; j < x; j++){
                if (i < oldSize && j < oldSize) {
                    plants[i][j] = oldGarden[i][j];
                    plants[i][j].updateObserver(pList[i][j]);
                }else{
                    plants[i][j] = new Tile(pList[i][j]);
                }
                plants[i][j].triggerUpdate(String.valueOf(gardenSize));
            }
        }

    }

    /**
     * Method: getDay()
     * Author: Jonathan McKee
     *
     * Returns the current day
     *
     * @return returns the current day count
     */
    public int getDay(){return dayCnt;}

    /**
     * Method: getDead()
     * Author: Jonathan McKee
     *
     * Returns the number of dead plants
     *
     * @return returns the number of plants that have died
     */
    public int getDead(){return deadCnt;}

    /**
     * Method: getAlive()
     * Author: Jonathan McKee
     *
     * Returns the number of alive plants
     *
     * @return returns the number of plants alive
     */
    public int getAlive(){return aliveCnt;}

    /**
     * Method: getGarden()
     * Author: Jonathan McKee
     *
     * Returns the current Garden
     *
     * @return returns the current garden
     */
    public Tile[][]getGarden(){return plants;}

    /**
     * Method: getSize()
     * Author: Jonathan McKee
     *
     * Returns the current garden size
     * @return
     */
    public int getSize(){return gardenSize;}


    /**
     * Method: Fertilize()
     * Author: Jonathan McKee
     *
     * Fertilize attempts to attempts to add fertilizer to a Tile square.  It first
     * checks to make sure a bag is still available, then checks if the bag is a
     * super grow bag, and then tells the tile to fertilize itself with the amount
     * of health to add.
     *
     * @param x       the x coordinate of the tile
     * @param y       the y coordinate of the tile
     * @param fertBag the fertilizer bag being used
     * @return the boolean false if the tile was not fertilized, true if it was
     */
    public boolean fertilize(int x, int y, int fertBag){
        double amount = 10;
        boolean fertilized = false;
        if (availFert[fertBag] <= 0){ //we ran out of fertilizer
            return fertilized;
        }
        //If miracle bag add 20
        if (fertBag == 2 )
            amount *= 2;

        fertilized = plants[y][x].fertilize(amount, fertBag == 2);
        if (fertilized)
            availFert[fertBag] -= 1;
        return fertilized;
    }

    /**
     * Method: newDay()
     * Author: Jonathan McKee
     *
     * This function runs through the motions for starting a new day.  We first fire off a
     * property change to observer signifying the day counter needs to update.  We then increment
     * the day counter, reset our fertilizer bags to available, reset our watering can back to 5
     * and run each tile's newday script.  If the tile returns a false a plant died and we increment
     * a temporary dead plant counter.  After checking all tiles we check to see if the counter has
     * incremented at all.  If above 0 we fire off two property changes one for the number of deadplants
     * and another for the number of planted plants.
     */
    public void newDay(){
        int tempDead = 0;
        //fire info change event
        subject.firePropertyChange("newDay", dayCnt, dayCnt + 1);
        //increment counter
        dayCnt += 1;
        //reset fertilizer
        availFert[0] = 1;
        availFert[1] = 1;
        availFert[2] = 1;
        waterCanLevel = 5;
        //System.out.println("Day: " + dayCnt); //Debug text
        //check every tile
        for (Tile[] row : plants){
            for(Tile current : row){
                if (!current.newDay()){
                    tempDead++;
                }
            }
        }
        if (tempDead > 0) {
            //notify died counter
            subject.firePropertyChange("plantDied", deadCnt, deadCnt + tempDead);
            deadCnt += tempDead;
            //notify alive counter
            subject.firePropertyChange("plantTotal", aliveCnt,  aliveCnt - tempDead);
            aliveCnt -= tempDead;
        }
    }

    /**
     * Method: plantSeed()
     * Author: Jonathan McKee
     *
     * This function attempts to plant a seed for a specific tile.  If the planting
     * is successful we fire off a property change to the observer for the planted plant
     * counter.
     *
     * @param x    the x value of the tile
     * @param y    the y value of the tile
     * @param type the type of seed (or till) to use.
     */
    public void plantSeed(int x, int y, int type){
        //Types 1-10 = flower, 11-20 = tree
        //1 daffodil
        //2 Rose
        //3 Tulip
        //11 pine
        //12 oak
        //13 cherry
        switch (type) {
            case 1:
                //System.out.println("Planting a flower at: " + x + "," + y);
                if (plants[y][x].plantFlower(1)) {
                    subject.firePropertyChange("plantTotal", aliveCnt, aliveCnt + 1);
                    aliveCnt++;
                }
                break;
            case 2:
                //System.out.println("Planting a flower at: " + x + "," + y);
                if (plants[y][x].plantFlower(2)){
                    subject.firePropertyChange("plantTotal", aliveCnt, aliveCnt + 1);
                    aliveCnt++;
                }
                break;
            case 3:
                //System.out.println("Planting a flower at: " + x + "," + y);
                if (plants[y][x].plantFlower(3)){
                    subject.firePropertyChange("plantTotal", aliveCnt, aliveCnt + 1);
                    aliveCnt++;
                }
                break;
            case 11:
                //System.out.println("Planting a tree at: " + x + "," + y);
                if(plants[y][x].plantTree(11)) {
                    subject.firePropertyChange("plantTotal", aliveCnt, aliveCnt + 1);
                    aliveCnt++;
                }
                break;
            case 12:
                //System.out.println("Planting a tree at: " + x + "," + y);
                if(plants[y][x].plantTree(12)) {
                    subject.firePropertyChange("plantTotal", aliveCnt, aliveCnt + 1);
                    aliveCnt++;
                }
                break;
            case 13:
                //System.out.println("Planting a tree at: " + x + "," + y);
                if(plants[y][x].plantTree(13)) {
                    subject.firePropertyChange("plantTotal", aliveCnt, aliveCnt + 1);
                    aliveCnt++;
                }
                break;
        }
    }


    /**
     * Method: useTool()
     * Author: Jonathan McKee
     *
     * This function attempts to use a tool on the specified plot.  If the shovel is used and removes
     * a plant we send off a property change to adjust the plant count.
     *
     * Current Tools:
     * Shovel - 0
     * Till - 1
     * Water Can - 2
     * Pruners - 3
     *
     * @param x    the x value of the tile
     * @param y    the y value of the tile
     * @param type the tool to use.
     */
    public void useTool(int x, int y, int type){
        switch (type) {
            case 1: //Till
                plants[y][x].useTill();
                break;
            case 2: //Water Can separated out for individual return
                //plants[y][x].water(5);
                break;
            case 3: //Pruners
                plants[y][x].prune(5);
                break;
            default: // Default to shovel
                //System.out.println("Removing plant at: " + x + "," + y);
                if(plants[y][x].useShovel()){
                    subject.firePropertyChange("plantTotal", aliveCnt,  aliveCnt - 1);
                    aliveCnt--;
                }
                break;
        }
    }


    /**
     * Method: setObserver()
     * Author: Jonathan McKee
     *
     * This function allows us to set a view observer for the Garden Moel to the custom observer
     * label class in the view package.
     *
     * @param observer the observer we want to add to the list.
     */
    public void setObserver(ObsLabel observer){
        subject.addPropertyChangeListener(observer);
    }


    /**
     * Method: triggerUpdate()
     * Author: Jonathan McKee
     *
     * This function triggers an update for all of the plant tiles.
     *
     */
    public void triggerUpdate(){
        for (int i = 0; i < plants.length; i++){
            for (int j = 0; j < plants.length; j++){
                plants[i][j].triggerUpdate(String.valueOf(gardenSize));
            }
        }
    }

    /**
     * Method: Water()
     * Author: Jonathan McKee
     *
     * This function adds the specified amount of water to all tiles.
     *
     * @param amount the amount of water to add
     */
    public void water(double amount){
        for(Tile[] x : plants){
            for(Tile y : x){
                y.water(amount);
            }

        }
    }

    /**
     * Method: Water()
     * Author: Jonathan McKee
     *
     * This overloaded version of the function allows us to add water to a specific plant.  For this
     * program, this is how we use the watering can on a specific plant.
     *
     * @param x      the x value of the tile
     * @param y      the y value of the tile
     * @param amount the amount of water to add
     * @return the number of watering can uses left
     */
    public int water(int x, int y, double amount){
        if (waterCanLevel <= 0)
            return 0;

        plants[y][x].water(amount);
        waterCanLevel--;
        return waterCanLevel;
    }
    //etc, "Hub class"
}

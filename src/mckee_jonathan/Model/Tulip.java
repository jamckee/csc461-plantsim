/** daffodil.java
 * Author: Jonathan McKee
 *
 * java class file to contain the flower plot.
 */
package mckee_jonathan.Model;

/**
 * The daffodil class extending from Plant.
 */
public class Tulip extends Plant{
    /**
     * Constructor: daffodil()
     * Author: Jonathan McKee
     * Instantiates a new daffodil.
     */
    public Tulip(){
        this.setHealth(10);
        this.setWater(15);
        this.setName("Tulip");
        this.setWatered(false);
        this.setPruned(false);
        this.setTilled(false);
    }

    /**
     * Method: toString()
     * Author: Jonathan McKee
     *
     * Overrides the default toString method to return the information
     * string of the class information.
     *
     * @return the new string
     */
    public String toString(){
        String temp = "Health:" + getHealth();
        return temp;
    }
}

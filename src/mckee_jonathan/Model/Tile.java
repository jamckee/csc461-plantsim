/** Tile.java
 * Author: Jonathan McKee
 *
 * Tile class file to contain our objects in the garden grid..
 */
package mckee_jonathan.Model;

import mckee_jonathan.View.PlantBtn;

import java.beans.PropertyChangeSupport;

/**
 * The Tile class.
 */
public class Tile {
    /**
     * The current soil moisture.
     */
    private double moisture = 0.0;
    /**
     * The Subject member value for firing messages to the observer of this class.
     */
    private PropertyChangeSupport subject;
    /**
     * The  type of plant currently on this tile.
     */
    private Plant type;

    /**
     * Constructor: Tile()
     * Author: Jonathan McKee
     *
     * Instantiates a new empty tile and links the passed in observer to the member variables listener list.
     *
     * @param observer the observer that will watch this tile
     */
    public Tile(PlantBtn observer) {
        subject = new PropertyChangeSupport(this);
        subject.addPropertyChangeListener(observer);
        type = new Empty();
        subject.firePropertyChange("created", type.getName(), this);
    }

    /**
     * Method: updateObserver()
     * Author: Jonathan McKee
     *
     * Updates this plant with a new plantbtn.
     *
     * @param observer
     */
    public void updateObserver(PlantBtn observer){
        subject = new PropertyChangeSupport(this);
        subject.addPropertyChangeListener(observer);
    }

    /**
     * Method: isFertilized()
     * Author: Jonathan McKee
     * This function checks if the plant has been fertilized
     *
     * @return true if fertilized
     */
    public boolean isFertilized(){
        return type.isFertilized();
    }
    /**
     * Method: isPruned()
     * Author: Jonathan McKee
     * This function checks if the plant has been Pruned
     *
     * @return true if Pruned
     */
    public boolean isPruned(){
        return type.isPruned();
    }
    /**
     * Method: isWatered()
     * Author: Jonathan McKee
     * This function checks if the plant has been watered
     *
     * @return true if watered
     */
    public boolean isWatered(){
        return type.isWatered();
    }

    /**
     * Method: Fertilize()
     * Author: Jonathan McKee
     * This function attempts to fertilize a specific Plant.  If this tile currently has an empty type, then the
     * returns false.  Otherwise the function will then fertilize the plant and fire a property change to display
     * no information for the this plant.
     *
     * @param amount the amount of health to add
     * @return the boolean true if we did fertilize false if we did not
     */
    public boolean fertilize(double amount, boolean supergrow){
        boolean val = false;
        //only fertilize plants
        if (type.getName().compareTo("None") != 0) {
            val = type.fertilize(amount, supergrow);
            subject.firePropertyChange("fertilize", type.getName(), this);
        }
        return val;
    }
    /**
     * Method: Prune()
     * Author: Jonathan McKee
     * This function attempts to prune a specific Plant.  If this tile currently has an empty type, then the
     * returns false.  Otherwise the function will prune the plant and fire a property change to display
     * new information for the this plant.
     *
     * @param amount the amount of health to add
     * @return the boolean true if we did fertilize false if we did not
     */
    public boolean prune(int amount){
        boolean val = false;
        if (type.getName().compareTo("None") != 0 && type.isPruned() == false) {
            val = type.prune(amount);
            subject.firePropertyChange("prune", type.getName(), this);

        }
        return val;
    }

    /**
     * Method: newDay()
     * Author: Jonathan McKee
     *
     * New day function for the Tile level.  We first call our plan'ts newDay function if we have one in this tile.
     * we then check to see if the plant is actually empty and return true if it was (To indicate the function ran
     * successfully).  We then subtract our daily moisture amount from the Tile itself.  The nexy check is to see if
     * the plant was killed or if the plant survived.  In both cases we simply fire a property change to notify the
     * observers that data has changed.
     *
     * @return false if the plan died, true otherwise.
     */
    public boolean newDay(){

        //decrease plant health by 5
        type.newDay(moisture);
        //return true even if no plant, to signify to pant was r
        if (type.getName().compareTo("None") == 0)
            return true;

        //check plant moisture requirements
                //handled in type.newDay
        //decrease soil moisture by 10
        moisture -= 10;
        if (moisture < 0)
            moisture = 0;
        //remove dead plants - pruneGarden();
        if (type.getHealth() <= 0){
            //remove plant
            type = new Empty();
            //fire info change
            subject.firePropertyChange("newday", type.getName(), this);
            return false;
        }else{ //Plant survived
        //fire info change
            subject.firePropertyChange("newday", type.getName(), this);
            return true;
        }
    }

    /**
     * Method: plantFlower()
     * Author: Jonathan McKee
     * This function allows us to plant a flower at the current tile location.  We then fire a property change to our
     * observers in order to update our counts.
     *
     * @return returns true if we were able to plant a flower.
     */
    public boolean plantFlower(int flowerType){
        boolean tilled = type.isTilled();
        if (flowerType == 1) {
            type = new Daffodil();
        } else if (flowerType == 2) {
            type = new Rose();
        } else if (flowerType == 3) {
            type = new Tulip();
        }
        moisture = 0;
        //Bonus for being tilled
        if (tilled){
            moisture += 5;
            type.addHealth(5);
        }
        subject.firePropertyChange("new", type.getName(), this);
        return true;
    }

    /**
     * Method: useTill()
     * Author: Jonathan McKee
     * This function allows us to till an empty plot.
     *
     * @return the boolean
     */
    public boolean useTill(){
        String oldTile = type.getName();
        //Only till a non-tilled tile
        if (oldTile.compareTo("None") == 0 && type.isTilled() == false){
            type = new Empty();
            type.setTilled(true);
            subject.firePropertyChange("new", "Tilled", this);
            return true;
        }
        return false;
    }

    /**
     * Method: useShovel()
     * Author: Jonathan McKee
     * This function allows us to clear a tile of any current plant and reset it back to Empty.
     *
     * @return the boolean
     */
    public boolean useShovel(){
        String oldTile = type.getName();
        moisture = 0;
        //if we replaced a flower or tree return true
        if (oldTile.compareTo("Empty") != 0 && type.isTilled() == false){
            type = new Empty();
            subject.firePropertyChange("new", type.getName(), this);
            return true;
        }else if (type.isTilled()){
            type.setTilled(false);
            subject.firePropertyChange("fillTill", type.getName(),this);
            return false; //not removing a plant so false
        }
        return false;
    }

    /**
     * Method: plantTree()
     * Author: Jonathan McKee
     * This function attempts to plant a tree at the current tile location. It will then fire a property change to
     * observers to display new counts for planted plants.
     *
     * @param treeType the type of tree to plant
     *
     * @return true after planting plant
     */
    public boolean plantTree(int treeType){
        boolean tilled = type.isTilled();
        if (treeType == 11)
            type = new Pine();
        else if (treeType == 12)
            type = new Oak();
        else if (treeType == 13)
            type = new Cherry();

        moisture = 0;
        if (tilled){
            moisture += 10;
            type.addHealth(10);
        }
        subject.firePropertyChange("new", type.getName(), this);
        return true;
    }

    /**
     * Method: toString()
     * Author: Jonathan McKee
     * Overrides the default toString method to return the information
     * string of the class information.
     *
     * @return the new string
     */
    public String toString(){
        String temp = type.toString();
        if (temp != "None"){
            temp +=  "\nSoil Moisture: " + moisture;
        }else{
            temp += "\n";
        }
        return temp;
    }

    /**
     * Method: Trigger update()
     * Author: Jonathan McKee
     *
     * Triggers a property change for all observers.
     */
    public void triggerUpdate(String str){
        subject.firePropertyChange(str, type.getName(), this);
    }

    /**
     * Method: Water()
     * Author: Jonathan McKee
     *
     * Waters this tile with the amount of water specified in the passed in parameter.
     *
     * @param amount the amount of water to add to soil moisture.
     */
    public void water(double amount){
        //only water if we're not an Empty tile
        if (type.getName().compareTo("None") != 0) {
            moisture += amount;
            type.setWatered(true);
            subject.firePropertyChange("water", type.getName(), this);
        }
    }
}

/** daffodil.java
 * Author: Jonathan McKee
 *
 * java class file to contain the flower plot.
 */
package mckee_jonathan.Model;

/**
 * The daffodil class extending from Plant.
 */
public class Daffodil extends Plant{
    /**
     * Constructor: daffodil()
     * Author: Jonathan McKee
     * Instantiates a new daffodil.
     */
    public Daffodil(){
        this.setHealth(20);
        this.setWater(10);
        this.setName("daffodil");
        this.setWatered(false);
        this.setPruned(false);
        this.setTilled(false);
    }

    /**
     * Method: toString()
     * Author: Jonathan McKee
     *
     * Overrides the default toString method to return the information
     * string of the class information.
     *
     * @return the new string
     */
    public String toString(){
        String temp = "Health:" + getHealth();
        return temp;
    }
}

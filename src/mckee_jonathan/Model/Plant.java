/** Plant.java
 * Author: Jonathan McKee
 *
 * Java class file that contains our base plant class.
 */
package mckee_jonathan.Model;

/**
 * The Plant parent class for our various types of plants.
 */
public class Plant {
    /**
     * The Health.
     */
    private double health;
    /**
     * The Water needs of the plant.
     */
    private double waterNeeds;
    /**
     * The Name, or type, of plant.
     */
    private String name;
    /**
     * A check to see if the plant has already been fertilized
     */
    private boolean fertilized;
    /**
     * A check to see if the plant has already been watered
     */
    private boolean watered;
    /**
     * A check to see if the plot has been tilled
     */
    private boolean tilled;
    /**
     * A check to see if the plant has already been pruned
     */
    private boolean pruned;


    /**
     * Method: Fertilize()
     * Author: Jonathan McKee
     *
     * This function checks to see if the plan is still alive, and if it is adds the specified amount
     * of health to the plant and then flags the plant as already fertilized.
     *
     * @param amount the amount of health to give the plant
     * @return if the fertilizing was successful
     */
    public boolean fertilize(double amount, boolean supergrow){
        if (health > 0 && !fertilized) {
            health += amount;
            fertilized = true;
            return true;
        } else if(health > 0 && fertilized && supergrow){
            //supergrow only 50% effective if already fertilized
            health += amount/2;
            fertilized = true;
            return true;
        }
        return false;
    }
    /**
     * Method: Prune()
     * Author: Jonathan McKee
     *
     * This function checks to see if the plant is still alive, and if it is adds the specified amount
     * of health to the plant and then flags the plant as already pruned.
     *
     * @param amount the amount of health to give the plant
     * @return if the pruning was successful
     */
    public boolean prune(int amount){
        if (health > 0) {
            health += amount;
            pruned = true;
            return true;
        }
        return false;
    }

    /**
     * Method: newDay()
     * Author: Jonathan McKee
     *
     * Runs our daily check on the plant's health.  We first mark the plant as no longer fertilized, we then
     * subtract the plant's daily 5 health, and check if the moisture value is above the plant's required moisture.  If
     * it is not we subtract an additional 10 health from the plant.
     *
     * @param moisture the moisture content of the tile holding the plant
     */
    public void newDay(double moisture){
        fertilized = false;
        watered = false;
        tilled = false;
        pruned = false;
        health -= 5;
        if (moisture < waterNeeds){
            health -= 10;
        }

    }
    /**
     * Method: addHealth()
     * Author: Jonathan McKee
     * Adds bonus health to the plant in the amount specified.
     *
     * @param amount the bonus health for the plant
     */
//setters
    protected void addHealth(double amount){
        health += amount;
    }
    /**
     * Method: setHealth()
     * Author: Jonathan McKee
     * Set health is a protected method that allows the specific plant's to set the health of their parent class.
     * Because the health is private, the inherited classes need to have a setter to access the health correctly.
     *
     * @param newHealth the new health of the plant
     */
    protected void setHealth(double newHealth){
        health = newHealth;
    }

    /**
     * Method: setWater()
     * Author: Jonathan McKee
     * Set water sets the water needs of the current plant tile.  Because of the privacy of the variable this function
     * is protected to allow sub-classes access to their own version of the variable.
     *
     * @param newWater the new water needs amount
     */
    protected void setWater(double newWater){
        waterNeeds = newWater;
    }

    /**
     * Method: setWatered()
     * Author: Jonathan McKee
     * Sets the watering state of plant
     *
     * @param status true if the plant was just watered
     */
    protected void setWatered(boolean status){
        watered = status;
    }
    /**
     * Method: setTilled()
     * Author: Jonathan McKee
     * Sets the tilled state of the plot
     *
     * @param status true if the plot is tilled
     */
    protected void setTilled(boolean status){
        tilled = status;
    }
    /**
     * Method: setPruned()
     * Author: Jonathan McKee
     * Sets the Pruned state of plant
     *
     * @param status true if the plant was just pruned
     */
    protected void setPruned(boolean status){
        pruned = status;
    }
    /**
     * Method: setFertilized()
     * Author: Jonathan McKee
     * Sets the Fertilized state of plant
     *
     * @param status true if the plant was just fertilized
     */
    protected void setFertilized(boolean status){
        fertilized = status;
    }

    /**
     * Method: setName()
     * Author: Jonathan McKee
     * Set name allows sub-classes to set the name of the Plant tile they are inherited from.
     *
     * @param newName the new name/type of the plant
     */
    protected void setName(String newName){
        name = newName;
    }

    /**
     * Method: getHealth()
     * Author: Jonathan McKee
     * Get health of the plant.
     *
     * @return the current health of the plant
     */

    public double getHealth(){
        return health;
    }

    /**
     * Method: getName()
     * Author: Jonathan McKee
     * Get name of the current plant
     *
     * @return the string name of the plant
     */
    public String getName(){
        return name;
    }

    /**
     * Method: isFertilized()
     * Author: Jonathan McKee
     * A check to see if the plant tile is fertilized.
     *
     * @return the fertilized boolean value.
     */
    public boolean isFertilized(){
        return fertilized;
    }
    /**
     * Method: isWatered()
     * Author: Jonathan McKee
     * A check to see if the plant tile is watered.
     *
     * @return the watered boolean value.
     */
    public boolean isWatered(){
        return watered;
    }
    /**
     * Method: isPruned()
     * Author: Jonathan McKee
     * A check to see if the plant tile is pruned.
     *
     * @return the pruned boolean value.
     */
    public boolean isPruned(){
        return pruned;
    }
    /**
     * Method: isTilled()
     * Author: Jonathan McKee
     * A check to see if the plant tile is tilled.
     *
     * @return the tilled boolean value.
     */
    public boolean isTilled(){
        return tilled;
    }

}
